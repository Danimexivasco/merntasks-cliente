import React, { Fragment, useState, useContext } from 'react';
import proyectoContext from '../../context/proyectos/proyectoContext';

const NuevoProyecto = () => {

    const proyectosContext = useContext(proyectoContext);
    const { nuevoProyecto, errorFormulario, mostrarFormulario, agregarProyecto, mostrarError } = proyectosContext;

    // State del proyecto
    const [proyecto, guardarProyecto] = useState({
        nombre: ''
    })

    // Extraemos nombre de proyecto
    const { nombre } = proyecto;

    const onChangeProyecto = e => {
        guardarProyecto({
            ...proyecto,
            [e.target.name]: e.target.value
        })
    }

    const onSubmitProyecto = e => {
        e.preventDefault()

        // Validar proyecto
        if (nombre.trim() === '') {
            mostrarError();
            return;
        }

        // Enviar el state
        agregarProyecto(proyecto)

        // Reiniciar el form
        guardarProyecto({
            nombre: ''
        })
    }

    return (
        <Fragment>
            <button
                type="button"
                className="btn btn-block btn-primario"
                onClick={() => mostrarFormulario()}
            >Nuevo Proyecto</button>

            {nuevoProyecto
                ?
                (
                    <form
                        className="formulario-nuevo-proyecto"
                        onSubmit={onSubmitProyecto}
                    >
                        <input
                            type="text"
                            className="input-text"
                            placeholder="Nombre proyecto"
                            name="nombre"
                            onChange={onChangeProyecto}
                        />

                        <input
                            type="submit"
                            className="btn btn-block btn-primario"
                            value="Añadir"
                        />


                    </form>
                )

                : null}
            {errorFormulario ? <p className="mensaje error">Es necesario introducir el nombre del Proyecto</p> : null}

        </Fragment>
    );
}

export default NuevoProyecto;