import React, { useContext, useEffect } from 'react';
import authContext from '../../context/autenticacion/authContext';
import proyectoContext from '../../context/proyectos/proyectoContext';

const Barra = () => {

    // Extraemos lo necesario del context
    const authsContext = useContext(authContext);
    const { usuario, usuarioAutenticado, cerrarSesion } = authsContext;

    const proyectosContext = useContext(proyectoContext);
    const {cerrarSesionProyectos} = proyectosContext;

    useEffect(() => {
        usuarioAutenticado();
        //eslint-disable-next-line
    },[])

    const cerrandoSesiones = () =>{
        cerrarSesion()
        cerrarSesionProyectos()
    }


    return (
        <header className="app-header">
            {usuario ? <p className="nombre-usuario">Hola <span>{usuario.nombre}</span></p> : null}
            <nav className="nav-principal">
                <button
                    className="btn btn-blank cerrar-sesion"
                    onClick={() => cerrandoSesiones()}
                >Cerrar Sesión</button>
            </nav>
        </header>
    );
}

export default Barra;