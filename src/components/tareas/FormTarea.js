import React, { useContext, useState, useEffect, Fragment } from 'react';
import proyectoContext from '../../context/proyectos/proyectoContext';
import tareaContext from '../../context/tareas/tareaContext';

const FormTarea = () => {

    const proyectosContext = useContext(proyectoContext)
    const { proyecto } = proyectosContext;

    const tareasContext = useContext(tareaContext);
    const {  agregarTarea, errorFormulario, tareaSeleccionada, validarTarea, obtenerTareas, actualizarTarea, cancelarEdicion } = tareasContext;


    useEffect(() => {
        if (tareaSeleccionada !== null) {
            guardarTarea(tareaSeleccionada)
        } else {
            guardarTarea({
                nombre: ''
            })
        }
    }, [tareaSeleccionada])

   

    // State de tareas 
    const [tarea, guardarTarea] = useState({
        nombre: ''
    });

    // Extraemos valores del State
    const { nombre } = tarea;

    // Si no hay proyecto seleccionado
    if (!proyecto) return null;

    // Array DESTRUCTURING para extraer el proyecto
    const [proyectoActual] = proyecto;

    // Checamos cambios input
    const handleChange = e => {
        guardarTarea({
            ...tarea,
            [e.target.name]: e.target.value
        })
    }
    

    // Submit del form 
    const onSubmit = e => {
        e.preventDefault()

        // Validacion del form
        if (nombre.trim() === '') {
            validarTarea();
            return;
        }

        if (tareaSeleccionada === null) {
            // Agregar nueva tarea al state
            tarea.proyecto = proyectoActual._id;
            // tarea.estado = false;
            agregarTarea(tarea)
        }else{
            actualizarTarea(tarea)
            cancelarEdicion()
        }

        // Mostramos todas las tareas del proyecto y las filtramos
        setTimeout(() => {
            obtenerTareas(proyectoActual._id)
        }, 200);
        

        // Reiniciar el form
        guardarTarea({
            nombre: ''
        })
    }

    // Funcion para cancelar edicion
    const cancelarEditar = () =>{
        cancelarEdicion()
    }

    

    return (
        <div className="formulario">
            <form
                onSubmit={onSubmit}
            >
                <div className="contenedor-input">
                    <input
                        type="text"
                        className="input-text"
                        placeholder="Nombre de la tarea"
                        name="nombre"
                        value={nombre}
                        onChange={handleChange}
                    />
                </div>
                <div className="contenedor-input-formulario">
                    {tareaSeleccionada ?
                        <Fragment>
                            <input
                                type="submit"
                                className="btn btn-primario btn-block btn-submit"
                                value="Editar Tarea"
                            />
                            <input
                                type="button"
                                className="btn btn-cancelar-form btn-block"
                                value="Cancelar"
                                onClick= {() => cancelarEditar()}
                            />
                        </Fragment>
                        :
                        <input
                            type="submit"
                            className="btn btn-primario btn-block btn-submit"
                            value="Agregar Tarea"
                        />

                    }

                </div>
                
            </form>
            {errorFormulario ? <p className="mensaje error">Es necesario un nombre para la tarea</p> : null}
        </div>
    );
}

export default FormTarea;