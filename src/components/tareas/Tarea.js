import React, { useContext } from 'react';
import proyectoContext from '../../context/proyectos/proyectoContext';
import tareaContext from '../../context/tareas/tareaContext';

const Tarea = ({ tarea }) => {


    const proyectosContext = useContext(proyectoContext)
    const { proyecto } = proyectosContext;

    const tareasContext = useContext(tareaContext);
    const { eliminarTarea, obtenerTareas, actualizarTarea, seleccionarTareaActual } = tareasContext;

    // Extraemos el proyecto actual
    const [proyectoActual] = proyecto;

    // Funcion para eliminar la tarea
    const tareaEliminar = id => {
        eliminarTarea(id, proyectoActual._id)
        obtenerTareas(proyectoActual._id)
    }

    // Cambiamos estado de la tarea
    const cambiarEstadoTarea = tarea => {
        if(tarea.estado){
            tarea.estado = false
        }else{
            tarea.estado = true
        }
        actualizarTarea(tarea)
    }

    // Seleccionamos la tarea a editar
    const seleccionarTarea = tarea => {
        seleccionarTareaActual(tarea)
        console.log(tarea.nombre)
    }

    return (
        <li className="tarea sombra">
            <p>{tarea.nombre}</p>
            <div className="estado">
                {tarea.estado
                    ?
                    (
                        <button
                            type="button"
                            className="completo"
                            onClick = {() => cambiarEstadoTarea(tarea)}
                        >Completada</button>
                    )
                    :
                    (
                        <button
                            type="button"
                            className="incompleto"
                            onClick = {() => cambiarEstadoTarea(tarea)}
                        >Incompleta</button>
                    )
                }
            </div>
            <div className="acciones">
                <button
                    type="button"
                    className="btn btn-primario"
                    onClick= {() => seleccionarTarea(tarea)}
                >Editar</button>
                <button
                    type="button"
                    className="btn btn-secundario"
                    onClick={() => tareaEliminar(tarea._id)}
                >Eliminar</button>
            </div>
        </li>
    );
}

export default Tarea;