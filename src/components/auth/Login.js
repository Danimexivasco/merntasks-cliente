import React, { useState, useContext, useEffect } from 'react';
import { Link } from 'react-router-dom';
import alertaContext from '../../context/alertas/alertaContext';
import authContext from '../../context/autenticacion/authContext';
import proyectoContext from '../../context/proyectos/proyectoContext';

const Login = (props) => {

    // Extraer los valores del context
    const alertasContext = useContext(alertaContext);
    const { alerta, mostrarAlerta } = alertasContext;

    const authsContext = useContext(authContext);
    const { mensaje, autenticado, iniciarSesion } = authsContext;

    const proyectosContext = useContext(proyectoContext);
    const { quitarMensaje } = proyectosContext;


    // En caso de que la contraseña o el usuario no sean correctos
    useEffect(() => {
        if (autenticado) {
            props.history.push('/proyectos');
        }
        if (mensaje) {
            mostrarAlerta(mensaje.msg, mensaje.categoria)
        }
        //eslint-disable-next-line
    }, [mensaje, autenticado, props.history])

    const [usuario, guardarUsuario] = useState({
        email: '',
        password: ''
    })

    // const [error, guardarError] = useState(false)
    const { email, password } = usuario;

    const leerLogin = e => {
        guardarUsuario({
            ...usuario,
            [e.target.name]: e.target.value
        })
    }

    const submitFormulario = e => {
        e.preventDefault();

        // Validar que no haya campos vacíos
        if (email.trim() === '' || password.trim() === '') {
            mostrarAlerta('Todos los campos son obligatorios', 'alerta-error');
            return;
        }

        // Quitamos mensajes previos de error si los hubiera
        quitarMensaje()
        
        // Pasarlo al action
        iniciarSesion({ email, password });
    }


    return (
        <div className="form-usuario">
            {alerta ? (<div className={`alerta ${alerta.categoria}`}>{alerta.msg}</div>) : null}
            <div className="contenedor-form sombra-dark">
                <h1>Iniciar Sesión</h1>

                <form
                    onSubmit={submitFormulario}
                >
                    <div className="campo-form">
                        <label htmlFor="email">Email</label>
                        <input
                            type="email"
                            id="email"
                            name="email"
                            placelholder="ejemplo@gmail.com"
                            value={email}
                            onChange={leerLogin}
                        />
                    </div>
                    <div className="campo-form">
                        <label htmlFor="password">Password</label>
                        <input
                            type="password"
                            id="password"
                            name="password"
                            placelholder="ejemplo@gmail.com"
                            value={password}
                            onChange={leerLogin}
                        />
                    </div>
                    <div className="campo-form">
                        <input type="submit" className="btn btn-primario btn-block" value="Iniciar Sesión" />

                    </div>

                </form>
                <Link to={'/nueva-cuenta'} className="enlace-cuenta">
                    Registrate
                </Link>
            </div>
        </div>
    );
}

export default Login;