import React , {useState, useContext, useEffect} from 'react';
import {Link} from 'react-router-dom';
import alertaContext from '../../context/alertas/alertaContext';
import authContext from '../../context/autenticacion/authContext';

const NuevaCuenta = (props) => {


    // Extraer los valores del context
    const alertasContext = useContext(alertaContext);
    const {alerta, mostrarAlerta} = alertasContext;

    const authsContext = useContext(authContext);
    const {mensaje, autenticado, registrarUsuario} = authsContext;

    // En caso de que el usuario se hay autenticado o registrado o sea un registro duplicado
    useEffect(()=>{
        if(autenticado){
            props.history.push('/proyectos');
        }
        if(mensaje){
            mostrarAlerta(mensaje.msg, mensaje.categoria)
        }
        //eslint-disable-next-line
    },[mensaje, autenticado, props.history])


    // State para iniciar sesión
    const [usuario, guardarUsuario] = useState({
        nombre:'',
        email:'',
        password:'',
        confirmar:''
    })

    // Extraemos del usuario
    const {nombre, email, password, confirmar} = usuario;

    const leerUsuario = e => {
        guardarUsuario({
            ...usuario,
            [e.target.name]: e.target.value
        })
    }

    const submitFormulario = e => {
        e.preventDefault();

        // Validacion de campos vacios 
        if(nombre.trim() === '' || email.trim() === '' || password.trim() === '' || confirmar.trim() === ''){
            mostrarAlerta('Todos los campos son obligatorios', 'alerta-error');
            return;
        }

        // Password minimo de 6 cracteres
        if(password.length < 4){
            mostrarAlerta('La contraseña debe de ser al menos de 4 carácteres', 'alerta-error'); 
            return;
        }
        // Comprobamos que las 2 contraseñas coinciden
        if(password !== confirmar){
            mostrarAlerta('Las contraseñas no coinciden', 'alerta-error'); ;
            return;
        }
        // Pasarlo al action
        registrarUsuario({
            nombre, email, password
        })
    }
    return ( 
        <div className="form-usuario">
            {alerta ? (<div className={`alerta ${alerta.categoria}`}>{alerta.msg}</div>) : null}
            <div className="contenedor-form sombra-dark">
                <h1>Registrarse</h1>

                <form
                onSubmit={submitFormulario}
                >
                    <div className="campo-form">
                        <label htmlFor="nombre">Nombre</label>
                        <input
                            type="text"
                            id="nombre"
                            name="nombre"
                            placelholder="Introduce tu nombre"
                            value={nombre}
                            onChange={leerUsuario}
                        />
                    </div>
                    <div className="campo-form">
                        <label htmlFor="email">Email</label>
                        <input
                            type="email"
                            id="email"
                            name="email"
                            placelholder="ejemplo@gmail.com"
                            value={email}
                            onChange={leerUsuario}
                        />
                    </div>
                    <div className="campo-form">
                        <label htmlFor="password">Password</label>
                        <input
                            type="password"
                            id="password"
                            name="password"
                            placelholder="ejemplo@gmail.com"
                            value={password}
                            onChange={leerUsuario}
                        />
                    </div>
                    <div className="campo-form">
                        <label htmlFor="confirmar">Confirmar Contraseña</label>
                        <input
                            type="password"
                            id="confirmar"
                            name="confirmar"
                            placelholder="ejemplo@gmail.com"
                            value={confirmar}
                            onChange={leerUsuario}
                        />
                    </div>
                    <div className="campo-form">
                        <input type ="submit" className="btn btn-primario btn-block" value="Regístrate"/>

                    </div>

                </form>
                <Link to={'/'} className="enlace-cuenta">
                    Iniciar Sesión
                </Link>
            </div>
        </div>
     );
}
 
export default NuevaCuenta;