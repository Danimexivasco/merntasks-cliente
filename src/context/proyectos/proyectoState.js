import React, { useReducer } from 'react';
// import {v4 as uuidv4} from 'uuid';

import proyectoContext from './proyectoContext';
import proyectoReducer from './proyectoReducer';
import { FORMULARIO_PROYECTO, OBTENER_PROYECTOS, AGREGAR_PROYECTO, PROYECTO_ERROR, VALIDAR_PROYECTO, PROYECTO_ACTUAL, ELIMINAR_PROYECTO, CERRAR_SESION, LOGIN_EXITOSO } from '../../types';
import clienteAxios from '../../config/axios';



const ProyectoState = props => {

    // const proyectos = [
    //     { id: 1, nombre: 'React' },
    //     { id: 2, nombre: 'Vue' },
    //     { id: 3, nombre: 'Laravel' },
    //     { id: 4, nombre: 'Dev' },
    // ]

    const initialState = {
        proyectos: [],
        nuevoProyecto: false,
        errorFormulario: false,
        proyecto: null,
        mensaje: null
    }

    // Dispatch para ejecutar las acciones
    const [state, dispatch] = useReducer(proyectoReducer, initialState)

    // Funciones para el CRUD
    const mostrarFormulario = () => {
        dispatch({
            type: FORMULARIO_PROYECTO
        })
    }

    // Obtener los proyectos
    const obtenerProyectos = async () => {
        try {
            const resultado = await clienteAxios.get('/api/proyectos');
            dispatch({
                type: OBTENER_PROYECTOS,
                payload: resultado.data.proyectos
            })
        } catch (error) {
            console.log(error)
            const alerta = {
                msg: 'Hubo un error',
                categoria: 'alerta-error'
            }
            dispatch({
                type: PROYECTO_ERROR,
                payload: alerta
            })
        }
        
    }

    // Agregar nuevo proyecto
    const agregarProyecto = async proyecto => {
        try {
            const resultado = await clienteAxios.post('/api/proyectos', proyecto);
            console.log(resultado)
            dispatch({
                type: AGREGAR_PROYECTO,
                payload: resultado.data
            })
        } catch (error) {
            console.log(error)
            const alerta = {
                msg: 'Hubo un error',
                categoria: 'alerta-error'
            }
            dispatch({
                type: PROYECTO_ERROR,
                payload: alerta
            })
        }

        // proyecto.id = uuidv4();
        // // Insertamos el proyecto en el State
        // dispatch({
        //     type:AGREGAR_PROYECTO,
        //     payload: proyecto
        // })
    }

    // Validamos el nuevo poryecto
    const mostrarError = () => {
        dispatch({
            type: VALIDAR_PROYECTO
        })
    }

    // Selecciona el proyecto que el usuario elijió
    const proyectoActual = proyectoId => {
        dispatch({
            type: PROYECTO_ACTUAL,
            payload: proyectoId
        })
    }

    // Eliminamos un proyecto
    const eliminarProyecto = async proyectoId => {
        try {
            await clienteAxios.delete(`/api/proyectos/${proyectoId}`)
            dispatch({
                type: ELIMINAR_PROYECTO,
                payload: proyectoId
            })
        } catch (error) {
            console.log(error)
            const alerta = {
                msg: 'Hubo un error',
                categoria: 'alerta-error'
            }
            dispatch({
                type: PROYECTO_ERROR,
                payload: alerta
            })
        }
    }

    // Cerramos sesion para que no muestre el ultimo proyecto abierto a la siguiente persona que se loguee
    const cerrarSesionProyectos = () => {
        try {
            dispatch({
                type: CERRAR_SESION,
            })
        } catch (error) {
            console.log(error)
        }
    }

    // Quitamos errores previos para que al loguearse no aparezca el mensaje almacenado de error
    const quitarMensaje = () => {
        try {
            dispatch({
                type: LOGIN_EXITOSO
            })
        } catch (error) {
            console.log(error)            
        }
    }

    return (
        <proyectoContext.Provider
            value={{
                proyectos: state.proyectos,
                nuevoProyecto: state.nuevoProyecto,
                errorFormulario: state.errorFormulario,
                proyecto: state.proyecto,
                mensaje: state.mensaje,
                mostrarFormulario,
                obtenerProyectos,
                agregarProyecto,
                mostrarError,
                proyectoActual,
                eliminarProyecto,
                cerrarSesionProyectos,
                quitarMensaje
            }}
        >
            {props.children}
        </proyectoContext.Provider>
    )
}

export default ProyectoState;