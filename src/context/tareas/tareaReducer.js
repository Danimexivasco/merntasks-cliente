import {TAREAS_PROYECTO, AGREGAR_TAREA, VALIDAR_TAREA, ELIMINAR_TAREA, TAREA_ACTUAL, ACTUALIZAR_TAREA, CANCELAR_EDICION} from '../../types';

export default (state,action) =>{
    switch(action.type){
        case TAREAS_PROYECTO:
            return{
                ...state,
                tareasProyecto: action.payload
                // tareasProyecto: state.tareasProyecto.filter(tarea=> tarea.proyectoId === action.payload)
            }
        case AGREGAR_TAREA:
            return{
                ...state,
                tareasProyecto: [action.payload, ...state.tareasProyecto ],
                errorFormulario: false
            }
        case VALIDAR_TAREA:
            return{
                ...state,
                errorFormulario: true
            }
        case ELIMINAR_TAREA:
            return{
                ...state,
                tareasProyecto: state.tareasProyecto.filter(tarea => tarea._id !== action.payload)
            }
        // case ESTADO_TAREA:
        case ACTUALIZAR_TAREA: 
            return{
                ...state,
                tareasProyecto: state.tareasProyecto.map(tarea => tarea._id === action.payload._id ? action.payload : tarea)
            }
        case TAREA_ACTUAL:
            return{
                ...state,
                tareaSeleccionada: action.payload
            }
        case CANCELAR_EDICION:
            return{
                ...state,
                tareaSeleccionada: null,
                
            }

        default:
            return state;
    }
}