import React, { useReducer } from 'react';
import tareaContext from './tareaContext';
import TareaReducer from './tareaReducer';
import { TAREAS_PROYECTO, AGREGAR_TAREA, VALIDAR_TAREA, ELIMINAR_TAREA, TAREA_ACTUAL, ACTUALIZAR_TAREA, CANCELAR_EDICION } from '../../types';
// import {v4 as uuidv4} from 'uuid';
import clienteAxios from '../../config/axios';



const TareaState = props => {
    const initialState = {
        // tareas: [
        //     { id:1, nombre: 'Elegir Hosting', estado: true, proyectoId: 1 },
        //     { id:2, nombre: 'Gestionar Cobro', estado: false, proyectoId: 2 },
        //     { id:3, nombre: 'MERN', estado: true, proyectoId: 3 },
        //     { id:4, nombre: 'Deploy', estado: false, proyectoId: 4 },
        //     { id:5, nombre: 'Elegir Hosting', estado: true, proyectoId: 2 },
        //     { id:6, nombre: 'Gestionar Cobro', estado: false, proyectoId: 3 },
        //     { id:7, nombre: 'MERN', estado: true, proyectoId: 4 },
        //     { id:8, nombre: 'Deploy', estado: false, proyectoId: 3 },
        //     { id:9, nombre: 'Elegir Hosting', estado: true, proyectoId: 3 },
        //     { id:10, nombre: 'Gestionar Cobro', estado: false, proyectoId: 1 },
        //     { id:11, nombre: 'MERN', estado: true, proyectoId: 2 },
        //     { id:12, nombre: 'Deploy', estado: false, proyectoId: 1 },
        //     { id:13, nombre: 'Elegir Hosting', estado: true, proyectoId: 3 },
        //     { id:14, nombre: 'Gestionar Cobro', estado: false, proyectoId: 4 },
        //     { id:15, nombre: 'MERN', estado: true, proyectoId: 4 },
        //     { id:16, nombre: 'Deploy', estado: false, proyectoId: 2 },
        // ],
        tareasProyecto: [],
        errorFormulario: false,
        tareaSeleccionada: null
    }

    // Creamos State y Dispatch
    const [state, dispatch] = useReducer(TareaReducer, initialState);


    // Definimos las  FUNCIONES
    // Agregar una nueva tarea 
    const agregarTarea = async tarea => {
        // console.log(tarea)
        // tarea.id= uuidv4();
        try {
            const resultado = await clienteAxios.post('../api/tareas', tarea);
            console.log(resultado.data.tarea)
            // console.log(resultado.data.nombre) 
            dispatch({
                type: AGREGAR_TAREA,
                payload: resultado.data.tarea
            })
        } catch (error) {
            console.log(error)
        }
    }
    
    // Obtener las tareas de un proyecto
    const obtenerTareas = async proyecto => {
        // console.log(proyecto)
        try {
            const resultado = await clienteAxios.get('../api/tareas', { params: { proyecto } })
            console.log(resultado.data.tareas)
            dispatch({
                type: TAREAS_PROYECTO,
                payload: resultado.data.tareas
            })
        } catch (error) {
            console.log(error)
        }
    }

    


    

    // Validamos el formulario de las tareas 
    const validarTarea = () => {
        dispatch({
            type: VALIDAR_TAREA
        })
    }

    // Eliminar la tarea seleccionada
    const eliminarTarea = async (id, proyecto) => {
        console.log(proyecto)
        try {
            await clienteAxios.delete(`/api/tareas/${id}`, {params: {proyecto}})
            dispatch({
                type: ELIMINAR_TAREA,
                payload: id
            })
        } catch (error) {
            console.log(error)
        }
    }

    
    // Seleccionamos la tarea a editar
    const seleccionarTareaActual = tarea => {
        dispatch({
            type: TAREA_ACTUAL,
            payload: tarea
        })
    }
    
    // Actualizamos la tarea
    const actualizarTarea = async tarea => {
        
        try {
            const resultado = await clienteAxios.put(`/api/tareas/${tarea._id}`, tarea);
            // console.log(resultado)
            dispatch({
                type: ACTUALIZAR_TAREA,
                payload: resultado.data.tarea
            })
        } catch (error) {
            console.log(error)    
        }
    }
    
    // Cambiar el estado de la tarea
    // const cambiarEstado = tarea => {
    //     dispatch({
    //         type: ESTADO_TAREA,
    //         payload: tarea
    //     })
    // }

    // Cancelamos la edición
    const cancelarEdicion = () => {
        dispatch({
            type: CANCELAR_EDICION
        })
    }


    return (
        <tareaContext.Provider
            value={{
                // tareas: state.tareas,
                tareasProyecto: state.tareasProyecto,
                errorFormulario: state.errorFormulario,
                tareaSeleccionada: state.tareaSeleccionada,
                obtenerTareas,
                agregarTarea,
                validarTarea,
                eliminarTarea,
                // cambiarEstado,
                seleccionarTareaActual,
                actualizarTarea,
                cancelarEdicion
            }}
        >
            {props.children}
        </tareaContext.Provider>
    )

}

export default TareaState;
